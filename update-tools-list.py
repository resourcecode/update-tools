#!/usr/bin/env python3
# coding: utf-8

import os
import json
from urllib.parse import quote_plus
from argparse import ArgumentParser

from gitlab import Gitlab


def get_mybinder_url(project):
    url = quote_plus(project.http_url_to_repo)
    return (
        f"https://mybinder.org/v2/git/{url}/HEAD?urlpath=voila%2Frender%2Findex.ipynb"
    )


def get_nbviewer_url(project):
    if "with-nbviewer" not in project.tag_list:
        return

    url = project.web_url[len("https://") :]
    return f"https://nbviewer.org/urls/{url}/-/raw/branch/default/index.ipynb"


def get_source_url(project):
    url = project.web_url
    return url + "/-/blob/branch/default/index.ipynb"


def get_example_url(project, preview_filepath):
    url = project.web_url
    return url + "/-/blob/branch/default/" + preview_filepath


def get_project_description(cnx, project):
    description = {
        "name": project.name.strip(),
        "description": project.description.strip(),
        "imageUrl": project.avatar_url,
        "binderUrl": get_mybinder_url(project),
        "nbviewerUrl": get_nbviewer_url(project),
        "sourceUrl": get_source_url(project),
    }

    preview_filepaths = [
        f["path"]
        for f in project.repository_tree(all=True)
        if f["name"].startswith("preview.")
    ]
    if preview_filepaths:
        description["exampleUrl"] = get_example_url(project, preview_filepaths[0])
    return description


def main():
    parser = ArgumentParser()
    parser.add_argument(
        "--gitlab-url",
        type=str,
        default=os.getenv("CI_SERVER_URL"),
    )
    parser.add_argument(
        "--gitlab-private-token",
        type=str,
        default=os.getenv("GITLAB_PRIVATE_TOKEN"),
    )
    parser.add_argument(
        "tools_group_id",
        type=int,
        help="The gitlab group id containing the tools repositories",
    )
    args = parser.parse_args()

    cnx = Gitlab(
        args.gitlab_url,
        private_token=args.gitlab_private_token,
    )

    tools = []
    group = cnx.groups.get(args.tools_group_id)
    for project in group.projects.list(as_list=False, all=True):
        project = cnx.projects.get(project.id)
        tools.append(get_project_description(cnx, project))

    print(json.dumps(tools))


if __name__ == "__main__":
    main()
