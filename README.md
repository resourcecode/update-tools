Update the tools list
=====================

Whenever a new tool has been added, or updated one should update the tools list.
This can be done following [this
link](https://gitlab.ifremer.fr/resourcecode/update-tools/-/pipelines/new) and
then clicking the “Run pipeline” button.
